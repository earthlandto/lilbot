#!/usr/bin/env python
# -*- coding: utf-8 -*-

import aiml
import sys
import module
import mtodo
import mgoogle
import mgoogleimage


aimlFiles = ["aparienciaFisica.aiml","infoBot.aiml","adjetivos.aiml","apelativos.aiml",
	"chistes.aiml","estadoAnimo.aiml","insultos.aiml","saludos.aiml",
	"astrologia.aiml","familia.aiml","nacionalidades.aiml","sinRespuestas.aiml",
	"atributosPsicologicos.aiml","nombre.aiml","conectores.aiml","genero.aiml",
	"profesiones.aiml","edad.aiml","infoUsuario.aiml",  "respuestaGenerales.aiml",
	"valve.aiml","nombres.aiml","todo.aiml", "google.aiml"]

# Chapuza:
def strip_accents(s):
	return s.replace('á','a').replace('é','e').replace('í','i').replace('ó','o').replace('ú','u').replace('Á','A').replace('É','E').replace('Í','I').replace('Ó','O').replace('Ú','U')
	
class chatbot:
	def __init__(self):

		self.k = aiml.Kernel()
		for f in aimlFiles:
			self.k.learn(f)
		self.k.setBotPredicate("name","Cristal")
		self.k.setBotPredicate("age","17")
		self.k.setBotPredicate("gender","mujer")
		self.k.setBotPredicate("location","Murcia")
		self.k.setBotPredicate("birthday","20 de Octubre del 2014")
		self.k.setBotPredicate("favoriteColor","rosa")
		self.k.setBotPredicate("footballteam","Real Betis")
		self.k.setBotPredicate("civilState","soltera")
		self.k.setBotPredicate("favouriteLanguage","Python")
		self.k.setBotPredicate("favoriteAuthor","Isaac Asimov")
		self.k.setBotPredicate("favoriteArtist","Chuck Norris")
		self.k.setBotPredicate("favoriteActress","Julia Roberts")
		self.k.setBotPredicate("favoriteSport","zekiball")

		self.modules = {}

	def respond(self, peer, msg):
		msg = strip_accents(msg)
		sentence = self.k.respond(msg,peer)
		session = self.k.getSessionData(peer)
		if ("system" in session.keys()):
			moduleToken = session["system"]
			self.k.setPredicate("system",None,peer)
			if (moduleToken in self.modules.keys()):
				mod = self.modules[moduleToken]
				if (mod.execute(self.k, peer, msg, sentence)):
					return mod.readOutput()
		return sentence

	def storeAllSessions(self):
		pass

	def loadAllSessions(self):
		pass

	def registerModule(self, mod):
		assert isinstance(mod, module.IModule)
		assert mod.getToken() not in self.modules.keys()
		self.modules[mod.getToken()] = mod

if (__name__ == "__main__"):
	tm = mtodo.moduleTodo()
	go = mgoogle.moduleGoogle()
	gi = mgoogleimage.moduleGoogleImage()
	bot = chatbot()
	bot.registerModule(tm)
	bot.registerModule(go)
	bot.registerModule(gi)
	line = ""
	for line in iter(sys.stdin.readline,''):
		print bot.respond("Bob",line)
		print gi.readOutput()
