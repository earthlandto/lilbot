import module
import aiml
import Queue

# Archivo a almacenar el TO-DO
todoFileName = "./todo.txt"

# Variables a leer de los aiml:
# Valor a incluir en el TO-DO
todoValueStr = "todo_value" # Si es nulo, se imprime el fichero TO-DO

class moduleTodo(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer, message, sentence):
        session = kern.getSessionData(peer)
        if (todoValueStr in session.keys()):
            val = session[todoValueStr]
            kern.setPredicate(todoValueStr,None,peer)
            if (val != ""):
                val = "> " + val
                self.store(val.strip())
                self.q.put("todo guardado");
        self.q.put("TODO:\n"+self.load())
        return True
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    def getToken(self):
        return "todo"

    def store(self, value):
        todoFile = open(todoFileName,"ab+")
        todoFile.write(value.encode('utf-8')+'\n')
        todoFile.close()
        
    def load(self):
        todoFile = open(todoFileName,"r+")
        value = todoFile.read()
        return value
        