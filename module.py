#!/usr/bin/env python
# -*- coding: utf-8 -*-

class IModule:
    # Se llama al módulo para ejecutar con el kernel, donde peer
    # es el indicador de la persona que lo ejecutó, sentence es la frase
    # que se generó como respuesta al usuario (esa frase no se manda), y message
    # el mensaje que envió el usuario que generó esta llamada.
    # Devuelve True si todo fue bien y además hay salida que leer,
    # o False en otro caso.
    def execute(self, kern, peer, message, sentence):
        pass
    
    # Devuelve la salida en orden FIFO de las ejecuciones 
    # Si no hay salida devuelve None
    def readOutput(self):
        return None
    
    # Devuelve el token que representa a este módulo
    def getToken(self):
        return ""
