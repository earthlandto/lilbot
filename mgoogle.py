#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
import json
import re
import module;
import Queue;

# Valores a pasar del aiml:
# Términos de la búsqueda
googleSearchStr = "google_search_terms" # Debe ser un string

def google(terms): 
    # q : terminos
    # hl : idioma
    query = urllib.urlencode ( { 'q' : terms , 'hl' : 'es'} )
    response = urllib.urlopen ( 'http://ajax.googleapis.com/ajax/services/search/web?v=1.0&' + query ).read()
    diccionario = json.loads(response)
    
    results = diccionario [ 'responseData' ] [ 'results' ]
    returnval=""
    for result in results:
        title = result['title']
        url = result['url']
        title = re.sub(u"<[bB]>","",title)
        title = re.sub(u"</[bB]>","",title)
        returnval += title + ' ; ' + url + '\n'
    return returnval.encode('utf-8')

class moduleGoogle(module.IModule):

    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer, message, sentence):
        session = kern.getSessionData(peer)
        if (googleSearchStr in session.keys()):
            val = session[googleSearchStr]
            kern.setPredicate(googleSearchStr,None,peer)
            if (val != ""):
                self.q.put(google(val))
                return True
        print "Error: google search without query terms"
        return False
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    
    def getToken(self):
        return "google"
