#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib
import json
import re
import module
import Queue
import random

# Valores a pasar del aiml:
# Términos de la búsqueda
googleImageSearchStr = "google_image_terms" # Debe ser un string

def gimage(terms): 
    # q : terminos
    # hl : idioma
    query = urllib.urlencode ( { 'q' : terms , 'hl' : "es", 'start' : str(random.randint(0,20)), "safe" : "off"} )
    response = urllib.urlopen ( 'https://ajax.googleapis.com/ajax/services/search/images?v=1.0&' + query ).read()
    diccionario = json.loads(response)
    
    results = diccionario [ 'responseData' ] [ 'results' ]

    return ["Estoy buscando alguna imagen, espera...",results[0]['unescapedUrl']]

class moduleGoogleImage(module.IModule):
    def __init__(self):
        self.q = Queue.Queue()

    def execute(self, kern, peer, message, sentence):
        session = kern.getSessionData(peer)
        if (googleImageSearchStr in session.keys()):
            val = session[googleImageSearchStr]
            kern.setPredicate(googleImageSearchStr,None,peer)
            if (val != ""):
                r = gimage(val)
                self.q.put(r[0])
                self.q.put(r[1])
                return True
        print "Error: google image search without query terms"
        return False
    
    def readOutput(self):
        if (self.q.empty()):
            return None
        return self.q.get()
    
    
    def getToken(self):
        return "googleimage"
